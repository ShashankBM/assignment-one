package main;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import java.io.IOException;
import java.util.*;
 

public class task1 {

 
	public void taskkill(String k) throws IOException
    {
    	
			if(k.equals("chrome")||k.equals("headless"))
			{
				
    		Runtime.getRuntime().exec("TASKKILL /f /im chromedriver.exe");
			
			}
			if(k.equals("firefox"))
			{
				Runtime.getRuntime().exec("TASKKILL /f /im geckodriver.exe");
			}
			if(k.equals("ie"))
			{
				Runtime.getRuntime().exec("TASKKILL /f /im IEDriverServer.exe");
			}
			if(k.equals("edge")) 
			{
				Runtime.getRuntime().exec("TASKKILL /f /im MicrosoftWebDriver.exe");
			}
		
    }

    public static void main(String[] args) throws IOException, InterruptedException {
       
    	 task1 ob=new task1();
        System.setProperty("webdriver.chrome.driver", "D:\\ALM image\\Sel\\chromedriver.exe");
        WebDriver dc=new ChromeDriver(); 
        dc.get("https://www.store.demoqa.com");
        Thread.sleep(5000);
        
        
        System.setProperty("webdriver.gecko.driver", "D:\\ALM image\\Sel\\geckodriver.exe");
        WebDriver df=new FirefoxDriver();
        df.get("https://www.store.demoqa.com");
        Thread.sleep(5000);
      
        
        System.setProperty("webdriver.ie.driver", "D:\\ALM image\\Sel\\IEDriverServer.exe");
        WebDriver die=new InternetExplorerDriver(); 
        die.get("https://www.store.demoqa.com");
        Thread.sleep(5000);
     
        
        System.setProperty("webdriver.edge.driver", "D:\\ALM image\\Sel\\MicrosoftWebDriver.exe");
        WebDriver de=new EdgeDriver();
        de.get("https://www.store.demoqa.com");
        Thread.sleep(5000);
        
        ChromeOptions cr = new ChromeOptions();
        cr.addArguments("headless");
        WebDriver dr=new ChromeDriver(cr);
        dr.get("https://www.store.demoqa.com");
        System.out.println(dr.getTitle());
        
        ob.taskkill("chrome");
        ob.taskkill("firefox");
        ob.taskkill("ie");
        ob.taskkill("edge");
        ob.taskkill("headless");
    }
}